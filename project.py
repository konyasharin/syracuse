"""
Модуль для CLI
"""
import argparse


def syracuse_sequence(n):
    """
    Генерация сиракузской последовательности.

    На вход подается начальное число в сиракузской последовательности,
    исходя из него генерируются числа по следующему принципу:
    если число четное, то оно делится пополам, иначе умножается на 3
    и прибавляется один и так пока не получится число 1.

    :param n: начальное число в сиракузской последовательности.
    :return: список чисел сиракузской последовательности.

    Примеры: 
    n = 3 ->
    -> [3, 10, 5, 16, 8, 4, 2, 1]
    n = 2 ->
    -> [2, 1]
    """
    list_numbers = [n]
    while n != 1:
        if not n % 2:
            n = n // 2
        elif n % 2:
            n = n * 3 + 1
        list_numbers.append(n)
    return list_numbers


def syracuse_max(n):
    """
    Поиск максимального числа в сиракузской последовательности.

    На вход подается начальное число в сиракузской последовательности,
    в функции syracuse_sequence генерируется сама последовательность и
    затем в ней идет поиск максимального числа без использования встроенной
    функции max, путем сравнения максимального встреченного числа со всеми.

    :param n: начальное число в сиракузской последовательности.
    :return: максимальное число в сиракузской последовательности.

    Примеры:
    n = 3 ->
    -> 16
    n = 2 ->
    -> 2
    """
    list_numbers = syracuse_sequence(n)
    max_number = list_numbers[0]
    for i in range(len(list_numbers)):
        if list_numbers[i] > max_number:
            max_number = list_numbers[i]
    return max_number


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--command', type=str,
                        help='Введите команду, которую хотите выполнить:\n'
                             'A-вывести сиракузскую последовательность для числа n\n'
                             'B-вывести максимальное число в сиракузской'
                             ' последовательности для числа n\n'
                             'C-выполнить обе команды')
    parser.add_argument('--number', type=int,
                        help='Введите число n-начальное число'
                             ' в сиракузской последовательности')
    args = parser.parse_args()
    if not args.command or (not args.number and args.number != 0):
        print('ОШИБКА! Введите все входные данные')
    else:
        command_dict = {'A': syracuse_sequence, 'B': syracuse_max}
        if args.number <= 0:
            print('Ошибка! число должно быть > 0')
        elif args.command == 'C':
            print(command_dict['A'](args.number), command_dict['B'](args.number), sep='\n')
        else:
            try:
                print(command_dict[args.command](args.number))
            except KeyError:
                print('ОШИБКА! Неверная команда')
